// Tic Tac Toe
// Use this main.cpp to test your TicTacToe class

#include <iostream>
#include <conio.h>

//#include "TicTacToe.h"

using namespace std;



class TicTacToe {

private:

	char m_board[3][3] = {
		{'1', '2', '3'},
		{'4','5','6'},
		{'7','8','9' }
	};

	int m_numTurns = 9;
	char m_playerTurn = 'X';
	char m_winner = ' ';

public:

	TicTacToe() {};

	void DisplayBoard()
	{
		cout << "\n";
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				cout << m_board[y][x] << " ";
			}
			cout << "\n";
		}
		cout << "\n";
	}

	bool IsOver()
	{
		if (m_numTurns == 0) return true;
		else if (m_board[0][0] == m_board[0][1] && m_board[0][1] == m_board[0][2]) return true;
		else if (m_board[1][0] == m_board[1][1] && m_board[1][1] == m_board[1][2]) return true;
		else if (m_board[2][0] == m_board[2][1] && m_board[2][1] == m_board[2][2]) return true;
		else if (m_board[0][0] == m_board[1][0] && m_board[1][0] == m_board[2][0]) return true;
		else if (m_board[0][1] == m_board[1][1] && m_board[1][1] == m_board[2][1]) return true;
		else if (m_board[0][2] == m_board[1][2] && m_board[1][2] == m_board[2][2]) return true;
		else if (m_board[0][0] == m_board[1][1] && m_board[1][1] == m_board[2][2]) return true;
		else if (m_board[0][2] == m_board[1][1] && m_board[1][1] == m_board[2][0]) return true;
		else return false;
	}

	char GetPlayerTurn()
	{
		return m_playerTurn;
	}

	bool IsValidMove(int position)
	{
		if ((m_board[GetYPosition(position)][GetXPosition(position)] == 'X') || (m_board[GetYPosition(position)][GetXPosition(position)] == 'Y')) return false;
		else return true;
	}

	void Move(int position)
	{
		m_board[GetYPosition(position)][GetXPosition(position)] = m_playerTurn;
		m_winner = m_playerTurn;
		m_numTurns -= 1;
		if (m_playerTurn == 'X') m_playerTurn = 'Y';
		else m_playerTurn = 'X';
	}

	void DisplayResult()
	{
		if (m_numTurns == 0) cout << "The game has ended in a tie." << "\n";
		else cout << "The Winner is: " << m_winner << "\n";
	}

	int GetXPosition(int position)
	{
		if (position == 1 || position == 4 || position == 7) return 0;
		else if(position == 2 || position == 5 || position == 8) return 1;
		else return 2;
	}
	int GetYPosition(int position)
	{
		if (position < 4) return 0;
		else if (position < 7 && position > 3) return 1;
		else return 2;
	}

};



int main()
{

	TicTacToe* pGame = nullptr;

	while (true)
	{
		if (pGame) delete pGame;
		pGame = new TicTacToe;

		// play the game
		while (!pGame->IsOver())
		{
			pGame->DisplayBoard();

			int position;
			do
			{
				cout << "Player " << pGame->GetPlayerTurn() << ", select a position (1-9): ";
				cin >> position;
			} while (!pGame->IsValidMove(position));

			pGame->Move(position);
		}

		// game over
		pGame->DisplayBoard();
		pGame->DisplayResult();


		// prompt to play again (or quit)
		char input = ' ';
		while (input != 'Y' && input != 'y')
		{
			std::cout << "Would you like to play again? (y/n): ";
			cin >> input;

			if (input == 'N' || input == 'n') return 0; // quit
		}
	}
	
}
